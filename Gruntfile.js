module.exports = function (grunt) {
	 
	var patterns = [
						{
							match: 'builddate',
							replacement: function() {
								return new Date().toLocaleString();
							}
						},
						{
							match: 'gitcommit',
							replacement: function() {
								var fullSHA = require('child_process').execSync('git rev-parse HEAD').toString().trim();
								return '<a href="https://gitlab.christophhaefner.de/web/cats/commit/'+fullSHA+'">'+fullSHA.substr(0,6)+'</a>';
							}
						},
						{
							match: 'buildauthor',
							replacement: function() {
								return process.env.USER;
							}
						},
						{
							match: 'buildmachine',
							replacement: function() {
								return require("os").hostname();
							}
						},
						{
							match: 'version',
							replacement: 'v<%= pkg.version %>'
						}
					];

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		clean: {
			dev: [ 'dev/**' ],
			beta: [ 'dist/**', '.tmp', 'build/**' ],
		},

		copy: {
			dev: {
				expand: true,
				cwd: 'app/',
				src: ['**'],
				dest: 'dev/'
			},
			devIcons: {
				files: [
					{src: 'app/images/cats-dev-128.png', dest: 'dev/images/cats-128.png'},
					{src: 'app/images/cats-dev-196.png', dest: 'dev/images/cats-196.png'},
					{src: 'app/images/cats-dev-256.png', dest: 'dev/images/cats-256.png'},
					{src: 'app/images/cats-dev-72-2x.png', dest: 'dev/images/cats-72-2x.png'},
					{src: 'app/images/cats-dev-72.png', dest: 'dev/images/cats-72.png'},
					{src: 'app/images/cats-dev-96.png', dest: 'dev/images/cats-96.png'}
				]
			},

			beta: {
				files: [
					{expand: true, dest: 'dist/', cwd: 'app/',
						src: ['index.html', 'favicon.ico', 'manifest.webmanifest', 'js/**', 'css/**', 'images/**/*.jpg', 'common/images/**']},
					{expand: true, dest: 'dist/fonts/', cwd: 'app/', src: ['common/fonts/**'], flatten: true}
				]
			},
			betaIcons: {
				files: [
					{src: 'app/images/cats-beta-128.png', dest: 'dist/images/cats-128.png'},
					{src: 'app/images/cats-beta-196.png', dest: 'dist/images/cats-196.png'},
					{src: 'app/images/cats-beta-256.png', dest: 'dist/images/cats-256.png'},
					{src: 'app/images/cats-beta-72-2x.png', dest: 'dist/images/cats-72-2x.png'},
					{src: 'app/images/cats-beta-72.png', dest: 'dist/images/cats-72.png'},
					{src: 'app/images/cats-beta-96.png', dest: 'dist/images/cats-96.png'}
				]
			},

			prodIcons: {
				files: [
					{src: 'app/images/cats-128.png', dest: 'dist/images/cats-128.png'},
					{src: 'app/images/cats-196.png', dest: 'dist/images/cats-196.png'},
					{src: 'app/images/cats-256.png', dest: 'dist/images/cats-256.png'},
					{src: 'app/images/cats-72-2x.png', dest: 'dist/images/cats-72-2x.png'},
					{src: 'app/images/cats-72.png', dest: 'dist/images/cats-72.png'},
					{src: 'app/images/cats-96.png', dest: 'dist/images/cats-96.png'}
				]
			}
		},

		includes: {
			options: {
				flatten: true,
				includePath: 'app/common'
			},
			dev: {
				dest: 'dev/',
				src: ['index.html'],
				cwd: 'app',
			},
			beta: {
				dest: 'dist/',
				src: ['index.html'],
				cwd: 'app',
			}
		},

		useminPrepare: {
			html: 'dist/index.html',
			options: {
				root: 'app/'
			}
		},

		usemin: {
			html: ['dist/index.html']
		},

		uglify: {
			options: {
				mangle: false // to preserve Angular.js-Injection
			}
		},

		htmlmin: {
			beta: {
				options: {
					removeComments: true,
					removeCommentsFromCDATA: true,
					removeCDATASectionsFromCDATA: true,
					collapseWhitespace: true
				},
				files: {
					'dist/index.html': 'dist/index.html'
				}
			}
		},

		replace: {
			async: {
				options: {
					patterns: [
						{
							match: 'data-async',
							replacement: 'async'
						}					]
				},
				files: [
					{expand: true, flatten: true, src: ['dist/index.html'], dest: 'dist/'}
				]

			},
			dev: {
				options: {
					patterns: patterns
				},
				files: [
					{expand: true, flatten: true, src: ['dev/index.html'], dest: 'dev/'}
				]
			},
			devPipeline: {
				options: {
					patterns: [{
							match: 'buildpipeline',
							replacement: 'dev'
						}]
				},
				files: [
					{expand: true, flatten: true, src: ['dev/index.html'], dest: 'dev/'}
				]
			},
			beta: {
				options: {
					patterns: patterns
				},
				files: [
					{expand: true, flatten: true, src: ['dist/index.html'], dest: 'dist/'}
				]
			},
			betaPipeline: {
				options: {
					patterns: [{
							match: 'buildpipeline',
							replacement: 'main'
						}]
				},
				files: [
					{expand: true, flatten: true, src: ['dist/index.html'], dest: 'dist/'}
				]
			}
		},

		watch: {
			watch: {
				files: ['app/**', 'Gruntfile.js'],
				tasks: ['development']
			}
		},

		imagemin: [{
				expand: true,
				cwd: 'dist/',
				src: ['**/*.jpg'],
				dest: 'dist/'
			}
		],

		uncss: {
			beta: {
				options: {
					ignore: [/label.*/, '.center-block', /cat.*/, /x-flipbox.*/, /btn.*/, /button.*/, /glyphicon.*/]
				},
				files: {
					'dist/css/cats.un.css': ['dist/index.html']
				}
			}
		},

		cssmin: {
			beta: {
				files: {
					'dist/css/cats.min.css': ['dist/css/cats.un.css']
				}
			}
		},

		critical: {
			beta: {
				options: {
					base: 'dist/',
					width: 260,
					height: 640
				},
				src: 'dist/index.html',
				dest: 'dist/index.html'
			}
		},

		'sw-precache': {
			options: {
				cacheId: '<%= pkg.name %>',
				workerFileName: 'sw.js'
			},

			dev: {
				options: {
					baseDir: "./dev",
					handleFetch: false
				},
				staticFileGlobs: [
					'common/images/**/*.{gif,png,jpg}'
				]
			},

			beta: {
				options: {
					baseDir: "./dist",
					handleFetch: true
				},
				staticFileGlobs: [
					'common/images/**/*.{gif,png,jpg}',
					'css/cats.min.css',
					'js/cats.min.js',
					'font/**/*.{woff,ttf,svg,eot}',
				]

			}
		},

		connect: {
			dev: {
				options: {
					port: 9000,
					base: 'dev',
					open: true
				}
			}
		}
	});

	// Load all files starting with `grunt-`
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// Tell Grunt what to do when we type "grunt" into the terminal
	grunt.registerTask('default', ['dev']);
	grunt.registerTask('dev', ['development', 'connect:dev', 'watch']);
	grunt.registerTask('development', ['clean:dev', 'copy:dev', 'copy:devIcons', 'includes:dev', 'replace:dev', 'replace:devPipeline', 'sw-precache:dev']);
	grunt.registerTask('beta', ['prod', 'copy:betaIcons']);
	grunt.registerTask('prod', ['clean:beta', 'copy:beta', 'copy:prodIcons', 'includes:beta',  'replace:async', 'useminChain', 'cssMangling', 'replace:beta', 'replace:betaPipeline', 'htmlmin:beta', 'imagemin', 'sw-precache:beta']);
	grunt.registerTask('useminChain', ['useminPrepare', 'concat:generated', 'uglify:generated', 'cssmin:generated', 'usemin']);
	grunt.registerTask('cssMangling', ['uncss:beta', 'cssmin:beta', 'critical:beta']);
};

