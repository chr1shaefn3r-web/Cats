var catsApp = angular.module('catsApp', ['firebase', 'ngCookies']);

catsApp.controller('CatsCtrl', ['$scope', '$firebase', '$cookies', function CatsCtrl($scope, $firebase, $cookies) {
	var FBURL = "https://catsdev.firebaseio.com/";
	var catsRef = new Firebase(FBURL);

	$scope.loginInputText = "";
	$scope.login = function() {
		if($scope.loginInputText) {
			var expires = new Date((new Date()).getTime() + 1000 * parseInt(42000) * 60 * 60 * 24);
			var cookie = "cats=" + escape($scope.loginInputText) + ";";
			cookie += "expires=" + expires.toGMTString() + ";";
			document.cookie = cookie;
		}
		var cookieCats = $cookies.cats || $scope.loginInputText;
		if(cookieCats) {
			var auth = new FirebaseSimpleLogin(catsRef, function(error, user) {
				if (error) {
					// an error occurred while attempting login
					$scope.$broadcast("login-out");
					console.log(error);
				} else if (user) {
					// user authenticated with Firebase
					$scope.$broadcast("login-in");
					console.log('User ID: ' + user.uid + ', Provider: ' + user.provider);
				} else {
					// user is logged out
					$scope.$broadcast("login-out");
					console.log('logged out');
				}
			});

			try {
				var authValues = JSON.parse(cookieCats);
				auth.login('password', {
					email: authValues.email,
					password: authValues.password,
					rememberMe: true
				});
			} catch (e) {
				console.log('caught exception', e);
				$scope.$broadcast("login-out");
			}
		} else {
			$scope.$broadcast("login-noCookies");
		}
	};
	$scope.clickLogin = function() {
		$scope.login();
	};
	setInterval(function() {
		window.document.location.reload(true);
	}, 2*60*60*1000); // every 2h

	$scope.cats = $firebase(catsRef); // Automatically syncs everywhere in realtime

	var connectedRef = new Firebase(FBURL+".info/connected");
	connectedRef.on("value", function(snap) {
		if (snap.val() === true) {
			$scope.$broadcast("connected");
		} else {
			$scope.$broadcast("disconnected");
		}
	});

	var authedRef = new Firebase(FBURL+".info/authenticated");
	authedRef.on("value", function(snap) {
		if (snap.val() === true) {
			$scope.$broadcast("login-in");
		} else {
			$scope.$broadcast("login-out");
			$scope.login();
		}
	});

	$scope.toggle = function(cat) {
		var catRef = new Firebase(FBURL + cat.id);
		var toggledCat = $scope.toggleCat(cat);
		catRef.transaction(function(currentVal) {
			return toggledCat;
		}, function(error, committed, snapshot) {
		  /*alert('Was there an error? ' + error);
		  alert('Did we commit the transaction? ' + committed);
		  alert('The final value is: ' + snapshot.val());*/
			if(!committed && error.message == "permission_denied") {
				$scope.$broadcast("loggedOut");
			}
		});
		var flipbox = angular.element("x-flipbox")[cat.id];
		if(toggledCat.toggled) {
			flipbox.showFront();
			return;
		}
		flipbox.showBack();
	};

	$scope.toggleCat = function(cat) {
		if(cat.toggled) {
			cat.toggled = false;
			cat.now = "label-success";
			cat.text = "Draußen";
			return cat;
		}
		cat.toggled = true;
		cat.now = "label-danger";
		cat.text = "Drinnen";
		return cat;
	};

	$scope.$watchCollection("cats", function() {
		var flipboxen = angular.element("x-flipbox");
		if(flipboxen) {
			for(i=0; i<flipboxen.length; i++) {
				var flipbox = flipboxen[i];
				if($scope.cats[i].toggled) {
					flipbox.showFront();
				} else {
					flipbox.showBack();
				}
			}
		}
	});
}]);

catsApp.directive('myRepeatDirective', function() {
  return function(scope, element, attrs) {
    if (scope.$last){
		var cat = scope.cats[scope.$index];
		var flipbox = angular.element(element).children()[0];
		if(cat.toggled) {
			flipbox.showFront();
			return;
		}
		flipbox.showBack();
		return;
    }
  };
});

catsApp.directive('loginIndicator', function() {
	return {
		restrict: 'E',
		template: '<button type="button" class="btn btn-default btn-login">' +
					'<span class="glyphicon glyphicon-exclamation-sign"></span>' +
				  '</button>',
		link : function(scope, element, attrs) {
			scope.$on("login-out", function(e) {
				element.css({"display" : ""});
			});
			scope.$on("login-in", function(e) {
				element.css({"display" : "none"});
			});
			scope.$on("login-noCookies", function(e) {
				element.contents().removeClass("btn-default");
				element.contents().removeClass("btn-primary");
				element.contents().addClass("btn-danger");
			});
		}
	};
});
catsApp.directive('loginInput', function() {
	return {
		restrict: 'E',
		template: '<input type="text" ng-model="loginInputText" >',
		link : function(scope, element, attrs) {
			scope.$on("login-out", function(e) {
				element.css({"display" : ""});
				console.log("[loginInput] login-out");
			});
			scope.$on("login-in", function(e) {
				element.css({"display" : "none"});
				console.log("[loginInput] login-in");
			});
			scope.$on("login-noCookies", function(e) {
				console.log("[loginInput] login-noCookies");
			});
		}
	};
});

catsApp.directive('connectivityIndicator', function() {
	return {
		restrict: 'E',
		template: '<span class="glyphicon glyphicon-transfer"></span>',
		link : function(scope, element, attrs) {
			scope.$on("disconnected", function(e) {
				element.css({"display" : ""});
			});
			scope.$on("connected", function(e) {
				element.css({"display" : "none"});
			});
		}
	};
});

