if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('sw.js').catch(function(ex) {
		console.warn(ex);
		console.warn('(This warning can be safely ignored outside of the production build.)');
	});
}

